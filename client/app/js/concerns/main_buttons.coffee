$ ->
  $mainButtons = $('.main_buttons')

  $mainButtons.on 'click', '.teambuilder_button', (e) ->
    PokeBattle.navigation.showTeambuilder()

  createChallengeButton
    eventName: "findBattle"
    button: $mainButtons.find('.find_battle')
    clauses: [
      Conditions.SLEEP_CLAUSE
      Conditions.EVASION_CLAUSE
      Conditions.SPECIES_CLAUSE
      Conditions.OHKO_CLAUSE
      Conditions.PRANKSTER_SWAGGER_CLAUSE
      Conditions.UNRELEASED_BAN
      Conditions.RATED_BATTLE
      Conditions.TIMED_BATTLE
    ]
  createChallengeButton
    eventName: "findBattleunranked"
    button: $mainButtons.find('.find_battle_non_ranked')
    clauses: [
      Conditions.SLEEP_CLAUSE
      Conditions.EVASION_CLAUSE
      Conditions.SPECIES_CLAUSE
      Conditions.OHKO_CLAUSE
      Conditions.PRANKSTER_SWAGGER_CLAUSE
      Conditions.TIMED_BATTLE
    ]
  createChallengeButton
    eventName: "findBattleRandom"
    button: $mainButtons.find('.find_battle_random')
    clauses: [
      Conditions.SLEEP_CLAUSE
      Conditions.EVASION_CLAUSE
      Conditions.SPECIES_CLAUSE
      Conditions.OHKO_CLAUSE
      Conditions.PRANKSTER_SWAGGER_CLAUSE
      Conditions.TIMED_BATTLE
    ]

  createChallengePaneNew
    populate: $mainButtons.find('.find_battle_select_team')


  $mainButtons.find('.find_battle').on 'challenge', ->
    $this = $(this)
    $this.find('.find-icon')
      .addClass('icon-spinner spinner-anim')
      .removeClass("icon-earth")

  $mainButtons.find('.find_battle_non_ranked').on 'challenge', ->
    $this = $(this)
    $this.find('.find-icon')
      .addClass('icon-spinner spinner-anim')
      .removeClass("icon-earth")

  $mainButtons.find('.find_battle_random').on 'challenge', ->
    $this = $(this)
    $this.find('.find-icon')
      .addClass('icon-spinner spinner-anim')
      .removeClass("icon-earth")

  $mainButtons.find('.display_credits').click ->
    $modal = PokeBattle.modal('modals/credits')
    $modal.find('.modal-footer button').first().focus()

# Depresss Find Battle once one is found
depressFindBattle = ->
  $mainButtons = $('.main_buttons')
  $button = $mainButtons.find('.find_battle')
  $button.removeClass("disabled")
  $button.find('.find-icon')
    .removeClass("icon-spinner spinner-anim")
    .addClass("icon-earth")
  $mainButtons.find('.find_battle_select_team .select').removeClass('disabled')

depressFindBattleUnranked = ->
  $mainButtons = $('.main_buttons')
  $button = $mainButtons.find('.find_battle_non_ranked')
  $button.removeClass("disabled")
  $button.find('.find-icon')
    .removeClass("icon-spinner spinner-anim")
    .addClass("icon-earth")
  $mainButtons.find('.find_battle_select_team .select').removeClass('disabled')

depressFindBattleRandom = ->
  $mainButtons = $('.main_buttons')
  $button = $mainButtons.find('.find_battle_random')
  $button.removeClass("disabled")
  $button.find('.find-icon')
    .removeClass("icon-spinner spinner-anim")
    .addClass("icon-earth")
  $mainButtons.find('.find_battle_select_team .select').removeClass('disabled')

$(window).load ->
  $mainButtons = $('.main_buttons')
  PokeBattle.battles.on 'add', (battle) ->
    if !battle.get('spectating')
      depressFindBattle()
      depressFindBattleUnranked()
      depressFindBattleRandom()

  PokeBattle.primus.on 'findBattleCanceled', depressFindBattle
  PokeBattle.events.on 'findBattleCanceled', depressFindBattle

  PokeBattle.primus.on 'findBattleCanceledUnranked', depressFindBattleUnranked
  PokeBattle.events.on 'findBattleCanceledUnranked', depressFindBattleUnranked

  PokeBattle.primus.on 'findBattleCanceledRandom', depressFindBattleRandom
  PokeBattle.events.on 'findBattleCanceledRandom', depressFindBattleRandom
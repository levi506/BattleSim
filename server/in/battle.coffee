coffee = require 'coffee-script'
path = require('path').resolve(__dirname, '../bw/battle.coffee')
eval(coffee.compile(require('fs').readFileSync(path, 'utf8'), bare: true))

{Ability} = require('./data/abilities')

@Battle::generation = 'in'

@Battle::actionMap['mega'] =
  priority: -> 5
  action: (action) ->
    @performMegaEvolution(action.pokemon)


#TODO :: Retrograde For Delta Ditto
@Battle::retrogradeMega = (pokemon) ->
  species = pokemon.species
  forme = "default"
  if(pokemon.forme.contains("mega"))
    pokemon.changeForme(forme)


@Battle::performMegaEvolution = (pokemon) ->
  if pokemon.species == "Rayquaza"
    species = "Rayquaza"
    forme = "mega"
  else
    [ species, forme ] = pokemon.item.mega
  pokemon.changeForme(forme)

  pokemon.team.megaEvolve() if /mega/.test(forme)
  pokemon.team.primalEvolve() if /primal/.test(forme)

  ability = @FormeData[species][forme]["abilities"][0]
  ability = Ability[ability.replace(/\s+/g, '')]
  pokemon.copyAbility(ability, reveal: false)
  # We set the original ability to this ability so that the ability
  # is not reset upon switch out.
  pokemon.originalAbility = ability
  pokemon.originalForme = forme

  # Generate and display mega-evolution message
  
  if species == 'Zoroark'
    alivemons = pokemon.team.getAlivePokemon()
    lastalivemon = alivemons[alivemons.length-1]
    possibleform = @FormeData[lastalivemon.species]
    if 'mega' of possibleform
      pieces = forme.split('-').map((s) -> s[0].toUpperCase() + s.substr(1))
      pieces.splice(1, 0, lastalivemon.species)
      megaFormeName = pieces.join(" ")
      @message "#{pokemon.name} Mega Evolved into #{megaFormeName}!"
    else 
      return
  else
    pieces = forme.split('-').map((s) -> s[0].toUpperCase() + s.substr(1))
    pieces.splice(1, 0, species)
    megaFormeName = pieces.join(" ")
    @message "#{pokemon.name} Mega Evolved into #{megaFormeName}!"

# Retrofit `recordMove` to also record mega evolutions.
oldRecordMove = @Battle::recordMove
@Battle::recordMove = (playerId, move, forSlot = 0, options = {}) ->
  pokemon = @getTeam(playerId).at(forSlot)
  if options.megaEvolve && !@getAction(pokemon) && pokemon.canMegaEvolve()
    if @pokemonActions.filter((o) -> o.type == 'mega' && o.pokemon.team == pokemon.team).length == 0
      @addAction(type: 'mega', pokemon: pokemon)
  oldRecordMove.apply(this, arguments)
